﻿using ProcessoSeletivTODOS.Clients.MVC.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessoSeletivTODOS.Clients.MVC.Controllers
{
    public class PerfilController : BaseController
    {
        // GET: Perfil
        public ActionResult Index()
        {
            return View(new PerfilViewModel());
        }

        public ActionResult List()
        {
            return PartialView(ConsomeApi<List<PerfilViewModel>>("api/Perfil",Method.GET, null).Data);
        }

        [HttpPost]            
        public ActionResult Salvar(PerfilViewModel vmodel)
        {
            if (!ModelState.IsValid)
                return View("Index", vmodel);

            var parameters = new Dictionary<string, object>();
            parameters.Add("PerfilId", vmodel.PerfilId);
            parameters.Add("Descricao", vmodel.Descricao);


            IRestResponse<PerfilViewModel> response;
            if(vmodel.PerfilId > 0)
                response = ConsomeApi<PerfilViewModel>("api/Perfil", Method.PUT, parameters);
            else
                response = ConsomeApi<PerfilViewModel>("api/Perfil", Method.POST, parameters);

            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent)
                SetNotificacaoSucesso("Perfil Adicionado com sucesso");
            else
                SetNotificacaoErro("Erro ao adicionar perfil !");

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var entity = ConsomeApi<PerfilViewModel>("api/Perfil/" + id.ToString(), Method.GET, null).Data;

            if(entity == null)
            {
                SetNotificacaoErro("Usuário indisponível ou inexistente !");
                return RedirectToAction("Index");
            }

            return View("Index", entity);
        }

        public ActionResult Delete(int id)
        {
            var response = ConsomeApi<PerfilViewModel>("api/Perfil/" + id.ToString(), Method.DELETE, null);

            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent)
                SetNotificacaoSucesso("Perfil Excluído com sucesso");
            else
                SetNotificacaoErro("Erro ao excluir perfil !");

            return RedirectToAction("Index");
        }
    }
}