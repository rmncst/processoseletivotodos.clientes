﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessoSeletivTODOS.Clients.MVC.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            HasError = false;
        }

        public bool HasError { get; set; }
        
        public IRestResponse<TResponse> ConsomeApi<TResponse>(string uri, Method method, IDictionary<string,object> parameters) where TResponse : new()
        {
            var client = new RestClient(System.Web.Configuration.WebConfigurationManager.AppSettings["api.resource"]);
            var request = new RestRequest(uri, method);

            parameters = parameters ?? new Dictionary<string, object>();

            foreach(var par in parameters)
            {
                request.AddParameter(par.Key , par.Value);
            }

            var response = client.Execute<TResponse>(request);
            if(response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                SetNotificacaoErro(response.ErrorMessage ?? "Erro ao conectar-se com a API");
            }
            return response;
        }

        public void SetNotificacaoSucesso(string mensage)
        {
            TempData["notificacao.success"] = mensage;
        }

        public void SetNotificacaoErro(string mensage)
        {
            HasError = true;
            TempData["notificacao.error"] = mensage;
        }

    }
}