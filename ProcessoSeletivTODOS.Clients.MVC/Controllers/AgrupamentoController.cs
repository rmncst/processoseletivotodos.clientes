﻿using ProcessoSeletivTODOS.Clients.MVC.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessoSeletivTODOS.Clients.MVC.Controllers
{
    public class AgrupamentoController : BaseController
    {
        // GET: Agrupamento
        public ActionResult Index()
        {
            var vmodel = new AgrupamentoViewModel
            {
                  Perfiis = ConsomeApi<List<PerfilViewModel>>("api/Perfil", Method.GET, null).Data
                , Usuarios = ConsomeApi<List<UsuarioViewModel>>("api/Usuario", Method.GET,null).Data                
            };

            return View(vmodel);
        }

        public ActionResult PartialPerfilUsuario(int iduser)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("iduser", iduser);
            parameters.Add("idprofile", 0);
            var vmodel = new PerfisUsuarioPartialViewModel
            {
                 Perfis = ConsomeApi<List<PerfilViewModel>>("api/Perfil",Method.GET,null).Data
                ,PerfisUsuario = ConsomeApi<List<UsuarioPerfilViewModel>>("api/UsuarioPerfil", Method.GET, parameters).Data
                ,User = ConsomeApi<UsuarioViewModel>("api/Usuario/" + iduser.ToString(), Method.GET, null).Data
                ,UsuarioId = iduser
            };
            return PartialView(vmodel);
        }
        
        [HttpPost]
        public void SalvaItem(int iduser, int idprofile)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("UsuarioId", iduser);
            parameters.Add("PerfilId", idprofile);

            ConsomeApi<bool>("api/UsuarioPerfil", Method.POST, parameters);
        }
        
        [HttpPost]
        public void DeletaItem(int iduser, int idprofile)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("iduser", iduser);
            parameters.Add("idprofile", idprofile);

            ConsomeApi<bool>("api/UsuarioPerfil", Method.DELETE, parameters);
        }
    }
}