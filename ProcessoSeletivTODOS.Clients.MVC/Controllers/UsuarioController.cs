﻿using ProcessoSeletivTODOS.Clients.MVC.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessoSeletivTODOS.Clients.MVC.Controllers
{
    public class UsuarioController : BaseController
    {
        // GET: Usuario
        public ActionResult Index()
        {
            return View(new UsuarioViewModel());
        }

        public ActionResult List()
        {
            return PartialView(ConsomeApi<List<UsuarioViewModel>>("api/Usuario", Method.GET, null).Data);
        }

        [HttpPost]
        public ActionResult Salvar(UsuarioViewModel model)
        {
            if (model.UsuarioId > 0)
            {
                ModelState.Remove("Senha");
                ModelState.Remove("ConfirmaSenha");
            }

            if (!ModelState.IsValid)
                return View("Index",model);
            
            if(model.Senha != model.ConfirmaSenha && model.UsuarioId == 0)
            {
                model.Senha = "";
                model.ConfirmaSenha = "";
                ModelState.AddModelError("ConfirmaSenha", "As senhas não coincidem !");
                return View("Index", model);
            }
            
            if(model.Login.Contains(" "))
            {
                ModelState.AddModelError("Login", "O Login do usuário não deve conter expeços !");
                return View("Index",model);
            }
            
            var parameters = new Dictionary<string, object>();
            parameters.Add("Ativo", model.Ativo);
            parameters.Add("Login", model.Login);
            parameters.Add("Email", model.Email);
            parameters.Add("DataCadastro", model.DataCadastro.ToShortDateString());
            parameters.Add("UsuarioId", model.UsuarioId);
            parameters.Add("Senha", model.Senha);

            if (model.UsuarioId > 0)
                ConsomeApi<UsuarioViewModel>("api/Usuario", Method.PUT, parameters);
            else
                ConsomeApi<UsuarioViewModel>("api/Usuario", Method.POST, parameters);

            if(!HasError)
                SetNotificacaoSucesso("Usuário salvo com sucesso !");

            return RedirectToAction("Index");
        }

        public ActionResult Editar(int id)
        {
            var vmodel = ConsomeApi<UsuarioViewModel>("api/Usuario/" + id.ToString(), Method.GET, null).Data;
            vmodel.ConfirmaSenha = vmodel.Senha;
            return View("Index", vmodel );
        }


        public ActionResult Delete(int id)
        {
            ConsomeApi<UsuarioViewModel>("api/Usuario/" + id.ToString(), Method.DELETE, null);
            SetNotificacaoSucesso("Usuário excluído com sucesso !");
            return RedirectToAction("Index");
        }
    }
}