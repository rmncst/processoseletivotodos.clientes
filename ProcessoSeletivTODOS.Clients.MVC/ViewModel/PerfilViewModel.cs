﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.MVC.ViewModel
{
    public class PerfilViewModel
    {
        public int PerfilId { get; set; }
        [Required(ErrorMessage ="Campo obrigatório !")]
        public string Descricao { get; set; }
    }
}