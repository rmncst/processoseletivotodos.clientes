﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.MVC.ViewModel
{
    public class PerfisUsuarioPartialViewModel
    {
        public int UsuarioId { get; set; }
        public UsuarioViewModel User { get; set; }
        public IEnumerable<PerfilViewModel> Perfis { get; set; }
        public IEnumerable<UsuarioPerfilViewModel> PerfisUsuario { get; set; }
    }
}