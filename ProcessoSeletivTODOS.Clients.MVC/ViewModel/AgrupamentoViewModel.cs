﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.MVC.ViewModel
{
    public class AgrupamentoViewModel
    {
        public AgrupamentoViewModel()
        {
            Usuarios = new List<UsuarioViewModel>();
        }

        public IEnumerable<UsuarioViewModel> Usuarios { get; set; }
        public IEnumerable<PerfilViewModel> Perfiis { get; set; }

        public int UsuarioId { get; set; }
        public int PerfilId { get; set; }
    }
}