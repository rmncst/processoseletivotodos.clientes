﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.MVC.ViewModel
{
    public class UsuarioViewModel
    {
        public UsuarioViewModel()
        {
            DataCadastro = DateTime.Now;
        }

        public int UsuarioId { get; set; }

        [Required(ErrorMessage ="Campo obrigatório !")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Campo obrigatório !")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo obrigatório !")]
        public bool Ativo { get; set; }

        [Required(ErrorMessage = "Campo obrigatório !")]
        public string Senha { get; set; }

        [Display(Name ="Confirma Senha")]
        [Required(ErrorMessage = "Campo obrigatório !")]
        public string ConfirmaSenha { get; set; }

        public DateTime DataCadastro { get; set; }
    }
}