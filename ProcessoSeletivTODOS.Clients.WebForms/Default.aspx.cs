﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProcessoSeletivTODOS.Clients.WebForms
{
    public partial class _Default : Page
    {
        public string EnderecoApi { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            EnderecoApi = System.Web.Configuration.WebConfigurationManager.AppSettings["api.resource"].ToString();
        }
    }
}