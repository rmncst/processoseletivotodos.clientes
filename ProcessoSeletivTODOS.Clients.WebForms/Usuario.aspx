﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Usuario.aspx.cs" Inherits="ProcessoSeletivTODOS.Clients.WebForms.Usuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <br />
            <h4>Gerenciamento de Usuários</h4>
            <br />
        </div>
    </div>
    <div class="form-horizontal">

        <asp:HiddenField ID="hdUsuarioId" runat="server" />

        <div class="form-group">
            <label class="col-md-2 control-label">Login</label>
            <div class="col-md-6">
                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" ></asp:TextBox>                
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-6">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ></asp:TextBox>                
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Ativo</label>
            <div class="col-md-6">
                <asp:CheckBox ID="chkAtivo" runat="server" ></asp:CheckBox>                
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Senha</label>
            <div class="col-md-6">
                <asp:TextBox ID="txtSenha" runat="server" CssClass="form-control"  type="password" ></asp:TextBox>                
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Confirmar Senha</label>
            <div class="col-md-6">
                <asp:TextBox ID="txtConfirmaSenha" runat="server" CssClass="form-control" type="password" ></asp:TextBox>                
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-2">
                <asp:Button runat="server" CssClass="btn btn-sm btn-primary" OnClick="Salvar" Text="Salvar"  />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-condensed " >
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Email</th>
                        <th>Ativo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        foreach (var item in Usuarios)
                        {
                         %>
                        <tr>
                            <td><%=item.Login%></td>
                            <td><%=item.Email%></td>
                            <td><%=item.Ativo%></td>
                            <td>
                                <a href="Usuario.aspx?mode=edit&userid=<%=item.UsuarioId%>" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="Usuario.aspx?mode=delete&userid=<%=item.UsuarioId%>" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

                            </td>
                        </tr>
                    <% } %>    
                </tbody>
            </table>
        </div>
    </div>
    
</asp:Content>

