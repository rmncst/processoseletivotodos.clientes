﻿using ProcessoSeletivTODOS.Clients.WebForms.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProcessoSeletivTODOS.Clients.WebForms
{
    public partial class Agrupamento : System.Web.UI.Page
    {
        public IEnumerable<PerfilModel> Perfis { get; set; }
        public int[] PerfisCadastrados { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var usuarios = Services.ApiService.ConsomeApi<List<UsuarioModel>>("api/Usuario", Method.GET, null).Data;
                if (usuarios == null)
                {
                    Session["erro"] = "Não foi possível conectar na API !";
                }
                else
                {
                    DropDownUsuarios.DataSource = usuarios;
                    DropDownUsuarios.DataValueField = "UsuarioId";
                    DropDownUsuarios.DataTextField = "Login";
                    DropDownUsuarios.DataBind();
                }
            }
        }

        protected void CarregaPerfis(object sender, EventArgs e)
        {
            Perfis = Services.ApiService.ConsomeApi<List<PerfilModel>>("api/Perfil", Method.GET, null).Data;
            var parameters = new Dictionary<string, object>();
            parameters.Add("idprofile", 0);
            parameters.Add("iduser", DropDownUsuarios.SelectedValue);
            var perfiisCad = Services.ApiService.ConsomeApi<List<UsuarioPerfil>>("api/UsuarioPerfil", Method.GET, parameters).Data;
            PerfisCadastrados = perfiisCad.Select(s => s.PerfilId).ToArray();
        }

        protected void SalvarItens(object sender, EventArgs e)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("idprofile", 0);
            parameters.Add("iduser", DropDownUsuarios.SelectedValue);
            var perfiisCad = Services.ApiService.ConsomeApi<List<UsuarioPerfil>>("api/UsuarioPerfil", Method.GET, parameters).Data;
            var todosPerfis = Services.ApiService.ConsomeApi<List<PerfilModel>>("api/Perfil", Method.GET, null).Data;

            //Excluindo os perfiis
            foreach (var profile in perfiisCad)
            {
                var profTemp = Request.Form["chk_" + profile.PerfilId.ToString()];
                if(profTemp == null)
                {
                    var parametersDelete = new Dictionary<string, object>();
                    parametersDelete.Add("iduser", DropDownUsuarios.SelectedValue);
                    parametersDelete.Add("idprofile", profile.PerfilId);

                    Services.ApiService.ConsomeApi<bool>("api/UsuarioPerfil", Method.DELETE, parametersDelete);
                }
            }

            //Incluindo os perfiis
            foreach(var profile in todosPerfis)
            {
                if(Request.Form["chk_" + profile.PerfilId.ToString()] != null)
                {
                    if(!perfiisCad.Select(s => s.PerfilId).Contains(profile.PerfilId))
                    {
                        var parametersInsert = new Dictionary<string, object>();
                        parametersInsert.Add("UsuarioId", DropDownUsuarios.SelectedValue);
                        parametersInsert.Add("PerfilId", profile.PerfilId);

                        Services.ApiService.ConsomeApi<bool>("api/UsuarioPerfil", Method.POST, parametersInsert);
                    }
                }
            }

            Session["notificacao"] = "Alterações realizadas com sucesso !";
        }
    }
}