﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.WebForms.Services
{
    public class ApiService
    {
        public static IRestResponse<TResponse> ConsomeApi<TResponse>(string uri, Method method, IDictionary<string, object> parameters) where TResponse : new()
        {
            var client = new RestClient(System.Web.Configuration.WebConfigurationManager.AppSettings["api.resource"]);
            var request = new RestRequest(uri, method);

            parameters = parameters ?? new Dictionary<string, object>();

            foreach (var par in parameters)
            {
                request.AddParameter(par.Key, par.Value);
            }

            var response = client.Execute<TResponse>(request);

            return response;
        }
        
    }
}