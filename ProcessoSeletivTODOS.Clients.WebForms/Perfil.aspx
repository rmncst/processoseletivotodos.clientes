﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="ProcessoSeletivTODOS.Clients.WebForms.Perfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <br />
            <h4>Gerenciamento de Perfis</h4>
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2">Descrição</label>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtDescricao" name="Descricao" runat="server" CssClass="form-control" ></asp:TextBox>        
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">
                        <asp:Button ID="Button1" OnClick="Salvar" CssClass="btn btn-sm btn-primary" runat="server" Text="Salvar"  />
                    </div>                    
                </div>
            </div>
            <asp:HiddenField ID="hdPerfilId" runat="server"  />
        </div>
    </div>
    <table class="table table-stripped" >
        <thead>
            <tr>
                <th>Descricao</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <%
                foreach (var item in Perfis)
                {
                 %>
                <tr>
                    <td><%=item.Descricao %></td>
                    <td>
                        <a href="Perfil.aspx?mode=edit&profileid=<%=item.PerfilId%>" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="Perfil.aspx?mode=delete&profileid=<%=item.PerfilId%>" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                    </td>   
                </tr>

            <% } %>
        </tbody>

    </table>
</asp:Content>
