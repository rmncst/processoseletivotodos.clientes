﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Agrupamento.aspx.cs" Inherits="ProcessoSeletivTODOS.Clients.WebForms.Agrupamento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <br />
            <h4>Agrupamento de Perfis e Usuários</h4>
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2">Selecione um usuário </label>
                    <div class="col-md-6">
                        <asp:DropDownList ID="DropDownUsuarios" CssClass="form-control" onchange="" runat="server"></asp:DropDownList>
                    </div>    
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">  
                        <asp:Button ID="btnCarregaPerfil" runat="server" Text="Buscar" CssClass="btn btn-sm btn-warning" OnClick="CarregaPerfis" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <br />
            <% if (Perfis != null && Perfis.Count() > 0)
                { %>
                <h4>Perfis do Usuário: <%=DropDownUsuarios.SelectedItem.Text%></h4>
            <% } %>
            <br />
            <table class="table table-stripped">
                <tbody>
                    <% if (Perfis != null && Perfis.Count() > 0)
                        {
                            foreach (var item in Perfis)
                            {
                                string arg = PerfisCadastrados.Contains(item.PerfilId) ? "checked=" : "";
                                %>
                                   <tr>
                                       <td><input type="checkbox" name="chk_<%=item.PerfilId%>" class="chk-action-t" value="<%=item.PerfilId %>" <%=arg %> /></td>
                                       <td><%=item.Descricao %></td>
                                   </tr> 
        
                                <% 
                            }
                        } %>
               </tbody>
           </table>
            <% if (Perfis != null && Perfis.Count() > 0)
            { %>
                <asp:Button Text="Salvar Itens" runat="server" CssClass="btn btn-primary btn-sm" OnClick="SalvarItens" />
            <%} %>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scriptsPage" runat="server">
    <script>
        $(document).ready(function () {
            $('.chk-action').change(function () {
                var method;
                var data;
                var value = $(this).val();
                
                if ($(this).prop('checked')) {
                    method = 'POST';
                    data = { UsuarioId: '<%=DropDownUsuarios.SelectedValue%>', PerfilId: value };
                }
                else {
                    method = 'DELETE';
                    data = { iduser: '<%=DropDownUsuarios.SelectedValue%>', idprofile: value };
                }
                console.log(method);
                $.ajax({
                    url: 'http://localhost:50127/api/UsuarioPerfil',
                    method: method,
                    data: data
                })

                toastr.success('Alterações realizadas com sucesso !')
            })
        })
    </script>
</asp:Content>
