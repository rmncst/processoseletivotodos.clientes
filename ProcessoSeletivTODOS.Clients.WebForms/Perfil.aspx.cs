﻿using ProcessoSeletivTODOS.Clients.WebForms.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProcessoSeletivTODOS.Clients.WebForms
{
    public partial class Perfil : System.Web.UI.Page
    {
        public IEnumerable<PerfilModel> Perfis { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Perfis = Services.ApiService.ConsomeApi<List<PerfilModel>>("api/Perfil", Method.GET, null).Data;
            if(Perfis == null)
            {
                Perfis = new List<PerfilModel>();
                Session["erro"] = "Impossível conectar na API";
            }

            if(Request.QueryString["mode"] == "edit" && !IsPostBack)
            {
                string id = Request.QueryString["profileid"];
                var perfil = Services.ApiService.ConsomeApi<PerfilModel>("api/Perfil/" + id, Method.GET, null).Data;

                txtDescricao.Text = perfil.Descricao;
                hdPerfilId.Value = perfil.PerfilId.ToString();
            }


            if (Request.QueryString["mode"] == "delete")
            {
                string id = Request.QueryString["profileid"];
                var perfil = Services.ApiService.ConsomeApi<PerfilModel>("api/Perfil/" + id, Method.DELETE, null);

                if(perfil.StatusCode == System.Net.HttpStatusCode.OK)
                    Session["notificacao"] = "Perfil excluido com sucesso !";
                else
                    Session["erro"] = "Erro ao excluir perfil. Para excluir um perfil, é necessário desvincular todos os usuário primeiros !";
                Response.Redirect("/Perfil.aspx");
            }
        }

        protected void Salvar(object sender, EventArgs e)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PerfilId", hdPerfilId.Value);
            parameters.Add("Descricao", txtDescricao.Text);

            if (hdPerfilId.Value != "")
                Services.ApiService.ConsomeApi<PerfilModel>("api/Perfil", Method.PUT, parameters);
            else
                Services.ApiService.ConsomeApi<PerfilModel>("api/Perfil", Method.POST, parameters);

            Session["notifcacao"] = "Perfil salvo com sucesso";

            Response.Redirect("~/Perfil.aspx");
        }
    }
}