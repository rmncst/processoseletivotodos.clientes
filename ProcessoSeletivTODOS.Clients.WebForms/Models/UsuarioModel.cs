﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.WebForms.Models
{
    public class UsuarioModel
    {
        public int UsuarioId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public bool Ativo { get; set; }
        public string Senha { get; set; }
        public string ConfirmaSenha { get; set; }
    }
}