﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.WebForms.Models
{
    public class PerfilModel
    {
        public int PerfilId { get; set; }
        public string Descricao { get; set; }
    }
}