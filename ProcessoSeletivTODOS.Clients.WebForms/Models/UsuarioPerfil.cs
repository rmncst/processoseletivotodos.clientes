﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivTODOS.Clients.WebForms.Models
{
    public class UsuarioPerfil
    {
        public int PerfilId { get; set; }
        public int UsuarioId { get; set; }
    }
}