﻿using ProcessoSeletivTODOS.Clients.WebForms.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcessoSeletivTODOS.Clients.WebForms.Services;

namespace ProcessoSeletivTODOS.Clients.WebForms
{
    public partial class Usuario : System.Web.UI.Page
    {
        public IEnumerable<UsuarioModel> Usuarios { get; set; }
                
        protected void Page_Load(object sender, EventArgs e)
        {
            Usuarios = ApiService.ConsomeApi<List<UsuarioModel>>("api/Usuario", Method.GET, null).Data;
            if(Usuarios == null)
            {
                Session["erro"] = "Não foi possível conectar na API";
                Usuarios = new List<UsuarioModel>();    
            }

            if(Request.QueryString["mode"] == "edit" && !IsPostBack)
            {
                string str = Request.QueryString["userid"];
                var user = ApiService.ConsomeApi<UsuarioModel>("api/Usuario/" + str, Method.GET, null).Data;

                if(user == null)
                {
                    Session["erro"] = "Não foi possível encontrar o usuário";
                }
                else
                {
                    txtUsuario.Text = user.Login;
                    txtEmail.Text = user.Email;
                    chkAtivo.Checked = user.Ativo;
                    hdUsuarioId.Value = user.UsuarioId.ToString();

                    txtSenha.Enabled = false;
                    txtConfirmaSenha.Enabled = false;
                }
            }

            if(Request.QueryString["mode"] == "delete")
            {
                string str = Request.QueryString["userid"];
                var user = ApiService.ConsomeApi<UsuarioModel>("api/Usuario/" + str, Method.DELETE, null).Data;
                Session["notificacao"] = "Usuario excluido com êxito";

                Response.Redirect("~/Usuario.aspx");
            }
        
        }
        
        protected void Salvar(object sender, EventArgs e)
        {
            var parameters = new Dictionary<string, object>();
            
            parameters.Add("Ativo", chkAtivo.Checked);
            parameters.Add("Login", txtUsuario.Text);
            parameters.Add("Email", txtEmail.Text);
            parameters.Add("UsuarioId", hdUsuarioId.Value);
            parameters.Add("Senha", txtSenha.Text);

            if (hdUsuarioId.Value != "")
                ApiService.ConsomeApi<UsuarioModel>("api/Usuario", Method.PUT, parameters);
            else
                ApiService.ConsomeApi<UsuarioModel>("api/Usuario", Method.POST, parameters);

            Session["notificacao"] = "Usuário salvo com sucesso !";
            Response.Redirect("~/Usuario.aspx");
        }
    }
}