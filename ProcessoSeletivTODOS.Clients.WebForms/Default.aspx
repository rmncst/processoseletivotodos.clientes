﻿<%@ Page Title="APP WebForms - TODOS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ProcessoSeletivTODOS.Clients.WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>
        Módulo WebForms
        <small>Processo Seletivo Todos</small>
        </h1>
        <p class="lead">
            Aplicação referente ao requisito de uma aplicação desenvolvido em Web Forms para consumir o Web Service criado.
            <br />
            Verificar se a API está rodando no seguinte endereço: <b><%=EnderecoApi%></b>. Caso não esteja, alterar no web config
        </p>
    </div>


</asp:Content>
