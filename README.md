Repositório referente as duas aplicações clientes solicitados no processo seletivo.

**Observações**

Antes de executar essas aplicações, conferir se a aplicação WEB API está rodando, e se a endereço da mesma está setado corretamente no webconfig das aplicações clientes.
O nome da chave é "app.resource"